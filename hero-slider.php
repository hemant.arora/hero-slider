<?php
/*
Plugin Name: Hero Slider
Plugin URI: http://tivlabs.com
Description: A smooth image slider for your website hero area with seamless transitions.
Version: 0.1
Author: Hemant Arora
Author URI: http://tivlabs.com/
*/

add_shortcode('hero_slider', 'hs_hero_slider');
function hs_hero_slider($atts) {
	global $post, $hs_slider_slide_index;
	$atts = shortcode_atts(array(
		'post_type'       => 'hero-slide',
		'order_by'        => 'date',
		'order'           => 'DESC',
		'count'           => 10,
		'id'              => '',
		'ar'              => 45,
		'height'          => null,
		'tpl'             => '',
		'auto_slide'      => 'true',
		'nav_arrows'      => 'false',
		'hover_sensitive' => 'true'
	), $atts, 'hs');
	extract($atts);
	$slide_posts_args = array(
		'posts_per_page'   => $count,
		'orderby'          => $order_by,
		'order'            => $order,
		'post_type'        => $post_type,
		'post_status'      => 'publish'
	);
	$slide_posts = get_posts($slide_posts_args);
	$html_slider = '';
	if(count($slide_posts) > 0) {
		$html_slider_slides = '<ul class="hero-slides">';
		$hs_slider_slide_index = -1;
		foreach($slide_posts as $post) {
			setup_postdata($post);
			$hs_slider_slide_index++;
			if(locate_template($tpl.'.php') != '') {
				ob_start();
				get_template_part($tpl);
				$html_slider_slides .= ob_get_clean();
			} else {
				$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
				$html_slider_slides .=
				'<li class="hero-slide post-'.get_the_ID().'"'.(empty($featured_image) ? '' : ' data-imagesrc="'.$featured_image[0].'"').'>'
					.'<h3>'.get_the_title().'</h3>'
				.'</li>';
			}
		}
		wp_reset_postdata();
		$html_slider_slides .= '</ul><!-- .hero-slides -->';
		$html_slider =
		'<div'.(empty($id) ? '' : ' id="'.$id.'"').' class="hero-slider loading-spinner'.($auto_slide == 'true' ? ' auto-slide' : '').($hover_sensitive == 'true' ? ' hover-sensitive' : '').'"'
			.' style="'.(is_null($height) ? (is_numeric($ar) ? 'height: auto; padding-top: '.$ar.'%;' : '') : 'height: '.(is_numeric($height) ? $height.'px' : $height).';').'">'
			.('true' == $nav_arrows ?
				'<div class="hs-nav-arrows">'
					.'<a href="javascript: void(0)" class="prev"></a>'
					.'<a href="javascript: void(0)" class="next"></a>'
				.'</div>'
			: '')
			.$html_slider_slides
		.'</div><!-- .hero-slider -->';
	} else {
		$html_slider = '<!-- failed to render hero-slider: no posts found -->';
	}
	return $html_slider;
}

add_action('wp_head', 'hs_wp_head');
function hs_wp_head() { ?>
<link rel="stylesheet" href="<?php echo plugins_url('hero-slider/css/hero-slider.css'); ?>" /><?php
}

add_action('wp_footer', 'hs_wp_footer');
function hs_wp_footer() { ?>
<script type="text/javascript" src="<?php echo plugins_url('hero-slider/js/hero-slider.js'); ?>"></script><?php
}
?>