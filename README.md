# Hero Slider (WordPress Plugin)

**Author:** Hemant Arora
**Company:** TivLabs

A responsive full width image slider that can be setup and configured
via shortcode.

### Shortcode Format

    [hero_slider
        
        post_type       (String) Slug of post type to pick items from
                        DEFAULT: 'hero-slide'
        
        order_by        (String) Order By (similar to WP_Query)
                        DEFAULT: 'date'
        
        order           (String) Order (similar to WP_Query)
                        DEFAULT: 'DESC'
        
        count           (Integer) Number of items to display
                        DEFAULT: 10
        
        id              (String) HTML element ID for the slider
                        DEFAULT: ''
        
        ar              (Integer) Aspect Ratio of the slider, this aspect-ratio
                        is maintained for all screens and devices (responsive)
                        For example:
                            For width to height ratio of 100:30,
                            please specify 30
                        DEFAULT: 45
        
        height          (Integer) Fixed height of the slider.
                        If this parameter is specified, 'ar' is ignored.
                        DEFAULT: null
        
        tpl             (String) File-name of custom HTML template for slide
                        content, placed in the active theme folder.
                        For example:
                            Say, you wish to use
                            '/wp-content/my-active-theme/tpl/my-hero-slide.php'
                            as a template for slides content, please specify
                            'tpl/my-hero-slide'
                        DEFAULT: ''
        
        auto_slide      (Boolean) If the slider should run automatically.
                        Slides will switch every 5 seconds
                        DEFAULT: true
        
        nav_arrows      (Boolean) If navigation arrows should be rendered.
                        DEFAULT: false
        
        hover_sensitive (Boolean) If slider should register hover events. This
                        is useful if 'auto_slide' is set to true and the slider
                        should stop sliding automatically if visitor is hovering
                        over a slide or the navigation buttons.
                        DEFAULT: true
        
    ]

### Example

    [hero_slider post_type="my-portfolio" count="15" ar="40"
        tpl="slide-portfolio" nav_arrows="true"]
