var hs_slide_transition_speed = 'medium',
	hs_slide_distance_px = 200,
	hs_slide_delay = 5000;

jQuery(document).ready(function() {
	
	hs_init();
	
	jQuery('body').on('click', '.hero-slider .hs-nav-arrows a', function(e) {
		e.preventDefault();
		hs_slide(jQuery(this).closest('.hero-slider'), jQuery(this).hasClass('next') ? 'next' : 'prev');
	});
	
	jQuery('body').on('click', '.hero-slider + ul.hs-nav-buttons li', function(e) {
		e.preventDefault();
		hs_slide(jQuery(this).closest('ul.hs-nav-buttons').prev('.hero-slider'), null, jQuery(this).index());
	});
	
});

function hs_init() {
	var $hero_sliders = jQuery('.hero-slider').not('.hs-initialized');
	$hero_sliders.each(function(i, hero_slider) {
		var $hero_slider = jQuery(hero_slider);
		$hero_slider.addClass('hs-initialized').after('<ul class="hs-nav-buttons"><li class="active"></li></ul>');
		for(var x = 1; x < $hero_slider.find('.hero-slide').length; x++)
			$hero_slider.next('.hs-nav-buttons').append('<li></li>');
		hs_preload_next_image($hero_slider);
		if($hero_slider.hasClass('auto-slide')) {
			$hero_slider.data('interval', setInterval(function() {
				hs_slide($hero_slider, 'next');
			}, hs_slide_delay));
		}
		$hero_slider.hover(
			function() {
				if(jQuery(this).hasClass('hover-sensitive'))
					clearInterval(jQuery(this).data('interval'));
			},
			function() {
				var $this = jQuery(this);
				if($this.hasClass('hover-sensitive')) {
					if($this.hasClass('auto-slide')) {
						clearInterval($this.data('interval'));
						$this.data('interval', setInterval(function() {
							hs_slide($this, 'next');
						}, hs_slide_delay));
					}
				}
			}
		);
		$hero_slider.next('.hs-nav-buttons').hover(
			function() {
				if(jQuery(this).prev('.hero-slider').hasClass('hover-sensitive'))
					clearInterval(jQuery(this).prev('.hero-slider').data('interval'));
			},
			function() {
				var $this = jQuery(this).prev('.hero-slider');
				if($this.hasClass('hover-sensitive')) {
					if($this.hasClass('auto-slide')) {
						clearInterval($this.data('interval'));
						$this.data('interval', setInterval(function() {
							hs_slide($this, 'next');
						}, hs_slide_delay));
					}
				}
			}
		);
	});
}

function hs_slide($hero_slider, dir, slide_to) {
	if($hero_slider.hasClass('hs-loaded')
	&& !$hero_slider.hasClass('hs-sliding')) {
		dir = typeof(dir) == 'undefined' ? 'next' : dir;
		var $hero_slide_current = $hero_slider.find('.hero-slides li.hero-slide.active');
		if(typeof(slide_to) != 'undefined') { if(slide_to == $hero_slide_current.index()) return; dir = slide_to < $hero_slide_current.index() ? 'prev' : 'next'; }
		$hero_slider.addClass('hs-sliding');
		$hero_slider.next('.hs-nav-buttons').find('li.active').removeClass('active');
		if(dir == 'next') {
			var $hero_slide_new = $hero_slide_current.next('li');
			$hero_slide_new = $hero_slide_new.length > 0 ? $hero_slide_new : $hero_slider.find('li.hero-slide:first-child');
			if(typeof(slide_to) != 'undefined') { $hero_slide_new = $hero_slider.find('li.hero-slide:nth-child('+(slide_to + 1)+')'); }
			$hero_slide_current
				.animate({ opacity: '0', left: '-'+hs_slide_distance_px+'px' }, hs_slide_transition_speed, function() {
					$hero_slide_current.removeClass('active').attr('style', 'background-image: '+$hero_slide_current.css('background-image'));
				});
			$hero_slide_new
				.css({ display: 'list-item', opacity: '0', left: hs_slide_distance_px+'px' })
				.animate({ opacity: '1', left: '0' }, hs_slide_transition_speed, function() {
					$hero_slide_new.addClass('active').attr('style', 'background-image: '+$hero_slide_new.css('background-image')+'; display: list-item;');
					$hero_slider.removeClass('hs-sliding');
					$hero_slider.next('.hs-nav-buttons').find('li').eq($hero_slide_new.index()).addClass('active');
				});
		} else {
			var $hero_slide_new = $hero_slide_current.prev('li');
			$hero_slide_new = $hero_slide_new.length > 0 ? $hero_slide_new : $hero_slider.find('li.hero-slide:last-child');
			if(typeof(slide_to) != 'undefined') { $hero_slide_new = $hero_slider.find('li.hero-slide:nth-child('+(slide_to + 1)+')'); }
			$hero_slide_current
				.animate({ opacity: '0', left: hs_slide_distance_px+'px' }, hs_slide_transition_speed, function() {
					$hero_slide_current.removeClass('active').attr('style', 'background-image: '+$hero_slide_current.css('background-image'));
				});
			$hero_slide_new
				.css({ display: 'list-item', opacity: '0', left: '-'+hs_slide_distance_px+'px' })
				.animate({ opacity: '1', left: '0' }, hs_slide_transition_speed, function() {
					$hero_slide_new.addClass('active').attr('style', 'background-image: '+$hero_slide_new.css('background-image')+'; display: list-item;');
					$hero_slider.removeClass('hs-sliding');
					$hero_slider.next('.hs-nav-buttons').find('li').eq($hero_slide_new.index()).addClass('active');
				});
		}
	}
}

function hs_preload_next_image($hero_slider) {
	var $slide = $hero_slider.find('li.hero-slide').not('.hs-preloaded').eq(0);
	if($slide.length == 0) {
		$hero_slider.addClass('hs-loaded').removeClass('loading-spinner'); /*console.log('Preloading COMPLETE ', $hero_slider);*/
		return;
	} /*console.log('Preloading ', $slide, ' of ', $hero_slider);*/
	var imagesrc = $slide.attr('data-imagesrc');
	if(typeof(imagesrc) == 'undefined') {
		$slide.addClass('hs-preloaded');
		hs_preload_next_image($hero_slider);
		if($slide.is(':first-child'))
			$slide.fadeIn('fast');
		return;
	}
	var img_obj = new Image();
	img_obj.src = imagesrc;
	img_obj.target_slide = $slide;
	img_obj.onload = function() {
		jQuery(this.target_slide).addClass('hs-preloaded').css({ backgroundImage: 'url('+this.src+')' }).removeAttr('data-imagesrc');
		hs_preload_next_image(jQuery(this.target_slide).closest('.hero-slider'));
		if(jQuery(this.target_slide).is(':first-child'))
			$slide.addClass('active').fadeIn('fast');
	};
}