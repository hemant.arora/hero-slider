<?php
	$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
?><li class="hero-slide post-<?php the_ID(); ?>" <?php echo empty($featured_image) ? '' : ' data-imagesrc="'.$featured_image[0].'"'; ?>>
	<h3><?php the_title(); ?></h3>
</li>
